SPIN=../bin/spin
INPUT=rybak.prml
MAXN=20000
PAN_CHECK=./pan -a -n -m$(MAXN)

runModel: $(INPUT)
	$(SPIN) $(INPUT)

check : pan
	$(PAN_CHECK) -N correctPartsSizes
	$(PAN_CHECK) -N correctBounds
	$(PAN_CHECK) -N sorted
	$(PAN_CHECK) -N correctSubarraySize
	ls

trail : *.trail
	./pan -r

pan : pan.c
	gcc -O2 pan.c -o pan

pan.c : $(INPUT)
	$(SPIN) -a $(INPUT)

clean :
	rm -f ./pan*
	rm -f ./*.trail

all : runModel check
